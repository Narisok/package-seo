<?php

namespace Narisok\Seo\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    // use HasFactory;

    protected $table = 'util_seos';

    protected $fillable = [];

    public function translate($key)
    {
        return $this->translations()->where('locale', '=', $key)->first();
    }

    public function translation()
    {
        $currentLocale = app()->getLocale();

        $trans = $this->hasOne(SeoTranslation::class, 'seo_id', 'id')->where('locale', '=', $currentLocale);

        return $trans;
    }

    public function translations()
    {
        return $this->hasMany(SeoTranslation::class, 'seo_id', 'id');
    }

    public function getTitleAttribute()
    {
        return $this->translation->title??null;
    }

    public function getDescriptionAttribute()
    {
        return $this->translation->description??null;
    }

    public function getKeywordsAttribute()
    {
        return $this->translation->keywords??null;
    }
}
