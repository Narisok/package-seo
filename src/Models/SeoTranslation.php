<?php

namespace Narisok\Seo\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeoTranslation extends Model
{
    // use HasFactory;

    protected $table = 'util_seo_translations';

    public $fillable = [
        'locale',
        'title',
        'description',
        'keywords'
    ];
}
