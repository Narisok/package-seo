@if (!($only_form ?? false))


    @isset($route)
    <form method="POST" action="{{ $route }}" enctype="multipart/form-data">
    @csrf
    @method($form_method ?? 'PUT')
    @endisset
    <div class="ibox {{ !($expanded ?? false) ? 'border-bottom' : '' }}">
        <div class="ibox-title ">
            <h5 class="collapse-link">
                {!! $title ?? 'Редагування СЕО' !!}
                @error('seo.*')
                    <span style="color: tomato;">{{ trans('translate.error') }}</span>
                @enderror
                @if(!($expanded ?? false)) <small class="m-l-sm">натисніть щоб відкрити </small>@endif
            </h5>
            @if (!($expanded ?? false))
            <div class="ibox-tools">
                <a class="collapse-link" >
                    <i style="color: green;" class="fa fa-chevron-up"></i>
                </a>
            </div>
            @endif
        </div>
        <div class="ibox-content " @if(!($expanded ?? false)) style="display: none;"  @endif>
@endif
@php
    $randKey = Str::uuid();
    $locales = config('seo.locales', [config('app.locale')]);
    $key = count($locales) == 1 ? $locales[0] : null;
@endphp
@if (!$key)
<div class="tabs-container">
    <ul class="nav nav-tabs">
        @foreach ($locales as $key)
            <li><a class="nav-link {{ $loop->last ? 'active' : '' }}" data-toggle="tab" href="#tab-{{ $key.$randKey }}"> <i class="fa fa-globe"></i> {{ $key }} @error(($name_prefix??'').$key.'.*') <span style="color: tomato;">{{ trans('translate.error') }}</span> @enderror</a></li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach ($locales as $key)
        <div id="tab-{{ $key.$randKey }}" class="tab-pane {{ $loop->last ? 'active' : '' }}">
            <div class="panel-body">

                    <div class="form-group  row">
                        <label class="col-sm-2 col-form-label">МЕТА заголовок</label>
                        <div class="col-sm-10">
                            <input type="text" name="seo[{{ $key }}][title]" class="form-control" value="{{ old('seo.'.$key.'.title') ?? ($seo ? $seo->translate($key)->title ?? '' : '') }}">
                            @error('seo.'.$key.'.title')
                                <label style="color: tomato;">{{ $message }}</label>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group  row">
                        <label class="col-sm-2 col-form-label">МЕТА опис</label>
                        <div class="col-sm-10">
                            <textarea type="text" rows="3" name="seo[{{ $key }}][description]" class="form-control">{{ old('seo.'.$key.'.description') ?? ($seo ? $seo->translate($key)->description ?? '' : '') }}</textarea>
                            @error('seo.'.$key.'.description')
                                <label style="color: tomato;">{{ $message }}</label>
                            @enderror
                        </div>
                    </div>

                    @if(!($disable_keywords??false))
                    <div class="form-group  row">
                        <label class="col-sm-2 col-form-label">МЕТА ключові слова <small>розділяйте за допомогою коми</small></label>
                        <div class="col-sm-10">
                            <input type="text" name="seo[{{ $key }}][keywords]" class="form-control" value="{{ old('seo.'.$key.'.keywords') ?? ($seo ? $seo->translate($key)->keywords ?? '' : '') }}">
                            @error('seo.'.$key.'.keywords')
                                <label style="color: tomato;">{{ $message }}</label>
                            @enderror
                        </div>
                    </div>
                    @endif

                </div>
            </div>
        @endforeach
    </div>
</div>
@else
<div class="form-group  row">
    <label class="col-sm-2 col-form-label">МЕТА заголовок</label>
    <div class="col-sm-10">
        <input type="text" name="seo[{{ $key }}][title]" class="form-control" value="{{ old('seo.'.$key.'.title') ?? ($seo ? $seo->translate($key)->title ?? '' : '') }}">
        @error('seo.'.$key.'.title')
            <label style="color: tomato;">{{ $message }}</label>
        @enderror
    </div>
</div>

<div class="form-group  row">
    <label class="col-sm-2 col-form-label">МЕТА опис</label>
    <div class="col-sm-10">
        <textarea type="text" rows="3" name="seo[{{ $key }}][description]" class="form-control">{{ old('seo.'.$key.'.description') ?? ($seo ? $seo->translate($key)->description ?? '' : '') }}</textarea>
        @error('seo.'.$key.'.description')
            <label style="color: tomato;">{{ $message }}</label>
        @enderror
    </div>
</div>
    @if(!($disable_keywords??false))
    <div class="form-group  row">
        <label class="col-sm-2 col-form-label">МЕТА ключові слова <small>розділяйте за допомогою коми</small></label>
        <div class="col-sm-10">
            <input type="text" name="seo[{{ $key }}][keywords]" class="form-control" value="{{ old('seo.'.$key.'.keywords') ?? ($seo ? $seo->translate($key)->keywords ?? '' : '') }}">
            @error('seo.'.$key.'.keywords')
                <label style="color: tomato;">{{ $message }}</label>
            @enderror
        </div>
    </div>
    @endif
@endif
@if (!($only_form ?? false))
    </div>
    @isset($route)
    <div class="ibox-footer">
        <div class="row ">
            <div class="col-lg-12 text-right">
                <button class="btn btn-primary btn-sm " type="submit">Зберегти</button>
            </div>
        </div>
    </div>
    @endisset
    </div>
    @isset($route)
    </form>
    @endisset

@endif
