<?php

namespace Narisok\Seo;

use Illuminate\Support\ServiceProvider;

class SeoProvider extends ServiceProvider
{

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'seo');

        $this->publishes([
            __DIR__.'/../config/seo.php' => config_path('seo.php')
        ], 'config');

        $this->publishes([
            __DIR__.'/../migrations/' => database_path('migrations')
        ], 'migrations');
    }
}
