<?php

namespace Narisok\Seo\Traits;

use Narisok\Seo\Models\Seo;

trait HasSeo
{
    // relationship
    public function seo()
    {
        return $this->morphOne(Seo::class, 'seoable');
    }


    // help functions
    public function updateSeo(array $attr) : bool
    {
        $locales = config('seo.locales', [config('app.locale')]);
        if($this->seo) {
            $this->seo->update();
            $seo = $this->seo;
        } else {
            $seo = $this->seo()->create();
        }
        foreach($locales as $locale) {
            if($attr[$locale] ?? false) {
                $seo->translations()->updateOrCreate([
                    'locale' => $locale,
                ], $attr[$locale]);
            }
        }
        return true;
    }

    public function destroySeo() : bool
    {
        $this->seo()->delete();
        return true;
    }

    
    public function getSeoTitleAttribute()
    {
        return str_replace(['{name}', '{category}', '{title}'], [$this->name ?? '', $this->category->name ?? '', $this->title ?? ''], $this->seo->title ?? $this->name ?? $this->title ?? '');
    }

    public function getSeoDescriptionAttribute()
    {
        return str_replace(['{name}', '{category}', '{title}'], [$this->name ?? '', $this->category->name ?? '', $this->title ?? ''], $this->seo->description ?? $this->description ?? $this->seo_title ?? '');;
    }

    public function getSeoKeywordsAttribute()
    {
        return $this->seo->keywords ?? $this->name ?? null;
    }
}
