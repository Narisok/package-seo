<?php

namespace Narisok\Seo\Traits;

trait SeoValidation
{

    public function makeSeoRules(bool $require_default = false)
    {

        // $currentLocale = app()->currentLocale();
        $defaultLocale = app()->getLocale();
        $locales = config('seo.locales', [config('app.locale')]);
        $rules = [];
        foreach($locales as $key) {
            $reqRule = ($require_default AND $key == ($defaultLocale == $key));

                $rules['seo.'.$key.'.title'] = [
                    $reqRule ? 'required' : 'nullable',
                    'string',
                    'max:300',
                ];
                $rules['seo.'.$key.'.description'] = [
                    $reqRule ? 'required' : 'nullable',
                    'string',
                    'max:15000',
                ];
                $rules['seo.'.$key.'.keywords'] = [
                    'nullable',
                    'string',
                    'max:1000',
                ];
        }
        return $rules;
    }
}
