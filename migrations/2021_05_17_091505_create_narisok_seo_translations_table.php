<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNarisokSeoTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('util_seo_translations', function (Blueprint $table) {
            $table->id();

            $table->foreignId('seo_id')->constrained('util_seos')->onDelete('CASCADE');
            $table->string('locale', 10)->index();

            $table->string('title', 300)->nullable();
            $table->text('description')->nullable();
            $table->string('keywords', 1000)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('util_seo_translations');
    }
}
